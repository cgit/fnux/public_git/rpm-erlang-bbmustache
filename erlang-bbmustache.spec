%global realname bbmustache
%global upstream soranoba

# Technically we're noarch, but our install path is not.
%global debug_package %{nil}

Name:     erlang-%{realname}
Version:  1.5.0
Release:  1%{?dist}
Summary:  Binary pattern match based Mustache template engine for erlang
License:  MIT
URL:      https://github.com/%{upstream}/%{realname}
Source0:  https://github.com/%{upstream}/%{realname}/archive/v%{version}/%{realname}-v%{version}.tar.gz
BuildRequires:  erlang-rebar
Requires:       erlang-rebar

%description
%{summary}.

%prep
%autosetup -n %{realname}-%{version}

%build
%{erlang_compile}

%install
%{erlang_install}

%check
%{erlang_test}

%files
%license LICENSE
%doc README.md doc/
%{erlang_appdir}/

%changelog
* Sun Jul 15 2018 Timothée Floure <fnux@fedoraproject.org> - 1.5.0-1
- Let there be package
